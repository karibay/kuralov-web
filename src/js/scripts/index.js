$(function() {
  if ($(".js-sticky")) {
    var sticky = new Sticky(".js-sticky");
  }

  // AOS.init({
  //   // Global settings:
  //   disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  //   startEvent: "DOMContentLoaded", // name of the event dispatched on the document, that AOS should initialize on
  //   initClassName: "aos-init", // class applied after initialization
  //   animatedClassName: "aos-animate", // class applied on animation
  //   useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  //   disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  //   debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  //   throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)

  //   // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
  //   offset: 400, // offset (in px) from the original trigger point
  //   delay: 0, // values from 0 to 3000, with step 50ms
  //   duration: 900, // values from 0 to 3000, with step 50ms
  //   easing: "ease-in-out", // default easing for AOS animations
  //   once: true, // whether animation should happen only once - while scrolling down
  //   mirror: false, // whether elements should animate out while scrolling past them
  //   anchorPlacement: "top-bottom" // defines which position of the element regarding to window should trigger the animation
  // });

  var wow = new WOW(
    {
      boxClass:     'wow',      // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset:       0,          // distance to the element when triggering the animation (default is 0)
      mobile:       true,       // trigger animations on mobile devices (default is true)
      live:         true,       // act on asynchronously loaded content (default is true)
      callback:     function(box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
      },
      scrollContainer: null,    // optional scroll container selector, otherwise use window,
      resetAnimation: true,     // reset animation on end (default is true)
    }
  );
  wow.init();

  var element = document.getElementById("phone");
  var maskOptions = {
    mask: "+{7} (000) 000-00-00"
  };
  if (element) {
    var mask = IMask(element, maskOptions);
  }

  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, "") ==
          this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          // event.preventDefault();
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            1000,
            function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {
                // Checking if the target was focused
                return false;
              } else {
                $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
            }
          );
        }
      }
    });

  const feedbackUrl = 'http://kuralov.com/api/feedbacks/';

  const faqUrl = 'http://kuralov.com/api/faq/'
  $.get(
      faqUrl,
      function(data){
        console.log(data);
        var i = 0;
        data.forEach(question => {
          i++;
          var collapsed = i==1?"":"collapsed";
          var show = i==1?"show":"";
          var expanded = i==1;
          var tag = `
          <div class="card">
            <div class="card-header" id="heading${i}">
              <div
                class="accordion-header ${collapsed}"
                data-toggle="collapse"
                data-target="#collapse${i}"
                aria-expanded="${expanded}"
                aria-controls="collapse${i}">
                <span>${question['question']}</span>
                <partial src="_rounded-arrow-icon.html"></partial>
              </div>
            </div>
          <div
          id="collapse${i}"
          class="collapse ${show}"
          aria-labelledby="heading${i}"
          data-parent="#accordionExample">
          <div class="card-body">${question['answer']}
          </div>
        </div>
      </div>`
          if (i<5) $(".accordion").before(tag);
          else $(".accordion-inner").before(tag);
          
        });
      }
      )


  $("#faq-form").submit(function(e) {
    e.preventDefault();

    var data = {
      name:$('#name').val(),
      phone_number:$('#phone').val(),
      text:$('#question').val()
    }
    $.post(
      feedbackUrl, data,
      function(response){
        $("#success-modal").modal("show");
      }
    );

  });
  // $('body').scrollspy({ target: '#scrollspy' })
});
